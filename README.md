# Zomato Restaurants in Sydney: A Data Analysis and Rating Prediction Project

### Table of Contents
1. [Introduction](#introduction)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Usage](#usage)
5. [Exploratory Data Analysis](#exploratory-data-analysis)
6. [Plots/Graphs](#plots-or-graphs)
7. [Feature Engineering](#feature-engineering)
8. [Regression Models](#regression-models)
9. [Classification Models](#classification-models)
10. [Docker](#docker)
11. [Conclusion](#conclusion)

### Introduction
This project aims to perform a detailed data analysis on restaurants in Sydney and also contains machine learning models that aim to predict the rating of restaurants listed on Zomato. We use data cleaning, feature engineering, and machine learning models for the prediction task. This README accompanies a Jupyter Notebook that contains all the code and visualizations.

### Technologies
Python 3.x <br>
Libraries: pandas, matplotlib, seaborn, scikit-learn, geopandas, Plotly, NumPy, etc.

### Installation

```json

git clone <repository_url> <br>
cd <directory> <br>
pip install -r requirements.txt

```
### Usage

- Run the Jupyter Notebook (data_understanding) to do EDA
- Run the Jupyter Notebook (feature_engineering) to train and evaluate the models.
- Run the Jupyter Notebook (u3237787_Mathachan_assignment1) to do both EDA and prediction.

    
### Exploratory Data Analysis
The notebook provides insights on:

- Unique cuisines served by Sydney restaurants.
- Suburbs with the highest number of restaurants.
- Relationship between restaurant ratings and their cost.

### Plots or Graphs
The following types of plots are used:

- Bar graphs
- Histograms
- Stacked Bar Charts
- Density Map

### Feature Engineering

Feature engineering has been done for the dataset.
- Drop rows where 'rating_number' is NaN
- Impute missing values in other columns
- Apply label encoding to categorical variables


### Regression models

- Normal Equation for Linear Regression
- Gradient Descent for Linear Regression

### Classification models

- Logistic Regression for Binary Classification
- Confusion Matrix for Evaluation

### Docker

You can find Dockerfile, .dockerignore and models.py file which are used to create a docker image of the predictive model. If you want to run the image in docker, make sure that you have docker desktop and docker hub access. In the command line, please follow the below steps.

```json
docker login -u USERNAME -p PASSWORD <br>
docker pull elizabeth98/prediction2023 <br>
docker run [IMAGE_ID]
```

### Conclusion

The notebook ends with descriptive insights about restaurant types, their ratings, and costs, as well as their geographical distribution in Sydney. In this project, I have successfully employed machine learning algorithms to predict the rating of restaurants listed on Zomato. The repository consists of the entire pipeline of the machine learning task, starting from feature engineering to modeling and evaluation. The docker image helps anyone to run the assignment easily just by docker run command.
