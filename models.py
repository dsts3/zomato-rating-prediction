#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Import all the dependencies required
import joblib
from joblib import load
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier


# In[2]:


#Load the data
df = pd.read_csv('data/data/zomato_df_final_data.csv')

# Drop rows where 'rating_number' is NaN since we want to predict this
df.dropna(subset=['rating_number'], inplace=True)
 
# Impute missing values for other columns
for col in df.columns:
    if df[col].dtype == "object":
        df[col].fillna(df[col].mode()[0], inplace=True)
    else:
        df[col].fillna(df[col].mean(), inplace=True)


# In[3]:


# Initialize Label Encoder
le = LabelEncoder()

# List of categorical columns to encode
categorical_cols = ['address', 'cuisine', 'link', 'phone', 'rating_text', 'subzone', 'title', 'type', 'color', 'cuisine_color']

# Apply Label Encoder
for col in categorical_cols:
    df[col] = le.fit_transform(df[col])


# In[4]:


# Select features and target variable
X = df.drop('rating_number', axis=1)
y = df['rating_number']

# Split data into training and test sets (80 and 20)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Initialize and train model
model_regression_1 = LinearRegression()
model_regression_1.fit(X_train, y_train)

# Predict and calculate MSE
y_pred_1 = model_regression_1.predict(X_test)
mse_1 = mean_squared_error(y_test, y_pred_1)


# In[5]:


# Initialize and train model
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

model_regression_2 = SGDRegressor()
model_regression_2.fit(X_train_scaled, y_train)

y_pred_2 = model_regression_2.predict(X_test_scaled)
mse_2 = mean_squared_error(y_test, y_pred_2)


# In[6]:


# Report the MSE value
print(f"MSE for model_regression_1: {mse_1}")
print(f"MSE for model_regression_2: {mse_2}")


# In[7]:


# Transform 'rating_text' into a binary classification target
df['rating_class'] = df['rating_text'].apply(lambda x: 1 if x in [0, 1, 4] else (2 if x in [2, 3] else None))


# In[8]:


# Select features and target variable
X = df.drop(['rating_number', 'rating_class', 'rating_text'], axis=1)
y = df['rating_class']

# Split data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Initialize and train model
model_classification_3 = LogisticRegression()
model_classification_3.fit(X_train, y_train)

# Make predictions
y_pred_3 = model_classification_3.predict(X_test)


# In[9]:


# Generate confusion matrix
conf_matrix = confusion_matrix(y_test, y_pred_3)
print(f"Confusion Matrix from the Logistic Regression:\n{conf_matrix}")


# In[10]:


# Initialize and train model
model_rf = RandomForestClassifier(n_estimators=100, random_state=0)
model_rf.fit(X_train, y_train)

# Make predictions
y_pred_rf = model_rf.predict(X_test)

# Generate confusion matrix and report performance
conf_matrix_rf = confusion_matrix(y_test, y_pred_rf)
print(f"Random Forest Confusion Matrix:\n{conf_matrix_rf}")
print(f"Random Forest Accuracy: {accuracy_score(y_test, y_pred_rf)}")


# In[11]:


# Initialize and train the model
model_dt = DecisionTreeClassifier(random_state=42)
model_dt.fit(X_train, y_train)

# Make predictions
dt_pred = model_dt.predict(X_test)

# Evaluate the model
conf_matrix_dt = confusion_matrix(y_test, dt_pred)
print(f"DT Confusion Matrix:\n{conf_matrix_dt}")
print(f"DT Accuracy: {accuracy_score(y_test, dt_pred)}")


# In[12]:


# Initialize and train model
model_knn = KNeighborsClassifier(n_neighbors=3)
model_knn.fit(X_train, y_train)

# Make predictions
y_pred_knn = model_knn.predict(X_test)

# Generate confusion matrix and report performance
conf_matrix_knn = confusion_matrix(y_test, y_pred_knn)
print(f"k-NN Confusion Matrix:\n{conf_matrix_knn}")
print(f"k-NN Accuracy: {accuracy_score(y_test, y_pred_knn)}")


# In[ ]:




